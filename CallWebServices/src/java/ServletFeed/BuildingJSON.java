/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ServletFeed;

import java.io.IOException;
import java.io.PrintWriter;
import javax.json.Json;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Vorndervile
 */
@WebServlet(name = "BuildingJSON", urlPatterns = {"/BuildingJSON"})
public class BuildingJSON extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        JSONObject jsMainplace1 = new JSONObject();
        JSONObject jsMainplace2 = new JSONObject();
        JSONObject author1 = new JSONObject();
        JSONObject author2 = new JSONObject();

        author1.put("petition", "พร พิรุณ");
        author1.put("meloday", "เอื้อ สุนทรสนาน");
        jsMainplace1.put("author", author1);
        jsMainplace1.put("title", "มาร์ช ม.ข.");
        jsMainplace1.put("content", "มหาวิทยาลัยขอนแก่น");

        author2.put("petition", "สุนทราภรณ์");
        author2.put("meloday", "สุนทราภรณ์");
        jsMainplace2.put("author", author2);
        jsMainplace2.put("title", "เสียงสนครวญ");
        jsMainplace2.put("content", "เสียงยอดสนอ่อนโอนพลิ้วโยนยอดไหว");
        JSONArray jsArray = new JSONArray();
        jsArray.put(jsMainplace1);
        jsArray.put(jsMainplace2);
        try (PrintWriter out = response.getWriter()) {
            out.println(jsArray);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
