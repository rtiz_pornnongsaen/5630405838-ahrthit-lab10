<%-- 
    Document   : index
    Created on : 12-Nov-2016, 7:08:11
    Author     : Vorndervile
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Lab : Calling and developing web services using Java</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
       <h2>Lab : Calling and developing web services using Java</h2>
        <ul><li><a href ="CallingFeed">Calling Feed</a> </li></ul>
        <ul><li><a href ="CallPTTWS">Calling PTT Web Services</a></li></ul>
        <ul><li><a href ="CallingIKKU">Calling iKKU</a></li></ul>
        <ul><li><a href ="CallingGoogleMap.html">Calling Google Map</a></li></ul>
        <ul><li><a href ="BuildingJSON">Building JSON Data</a></li></ul>
    </body>
</html>
